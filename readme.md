# Mars Surveillance Robot

The Olympic Channel has recently developed a Mars Surveillance Robot with the intent of exploring the surface on Mars and explore suitability for future Olympic Sports on low gravity over there. 

#### Built With

* 	[Maven Wrapper](https://github.com/takari/maven-wrapper) - Dependency Management
* 	[OpenJDK 11](https://openjdk.java.net/projects/jdk/11/) - Java™ Platform 
* 	[Spring Boot](https://spring.io/projects/spring-boot) - Framework to ease the bootstrapping and development of new Spring Applications
* 	[git](https://git-scm.com/) - Free and Open-Source distributed version control system
* 	[Lombok](https://projectlombok.org/) - Never write another getter or equals method again, with one annotation your class has a fully featured builder, Automate your logging variables, and much more.
* 	[Docker](https://www.docker.com/) - Containerization
* 	[Google JIB](https://github.com/GoogleContainerTools/jib) - Containerize your Java application

#### External Tools Used

* [IntelliJ IDEA](https://www.jetbrains.com/idea/download/)
* [Gitlab](https://about.gitlab.com/)

#### Operating System
* [Ubuntu 20.04](https://ubuntu.com/download)

### Requirements:
* You need to have Java 11 installed.
* You need to have git installed (You can download it from gitlab too)

## Project Structure

Maven multi module project:  

marsrobot (packaged as POM)  
├────────── marsrobot-commandline (packaged as JAR)  
├────────── marsrobot-rest (packaged as JAR)  
├────────── marsrobot-common (packaged as JAR)

##### marsrobot-commandline:
* Contains the command line application layer.

##### marsrobot-rest:
* Contains the rest interface.

##### marsrobot-common:
* Contains the logic and models shared in command line and rest applications. 

## Solution Definition

The business logic of the application (the robot and its actions) is done by the Robot.java class. It has the 4 operations that the robot needs to do to execute each command:
```java
public interface RobotActions {
    void applyMoveAndActions(Command command);
    boolean nextPositionIsInsideOfTerrainLimitsAndItIsNotAnObstacle();
    void calculateNextPosition(Command command);
    void applyBackOff() throws NoBackOffStrategyFoundException;
}
```

The proposed solution implements an algorithm in both rest and command line which for each command for the robot:
```
calculateNextPosition();
if (next_position is within terrain and it is not an obstacle) {
    applyMoveAndActions(command);
} else {
    applyBackOff();
}
``` 

The solution is implemented in a service layer class (MarsrobotService.java), which has a unique public method to prepare and start the robot.  
For input validations, we use package javax.validation.constraints, being applied automatically in Rest using @Valid annotation on input DTO, and triggered manually in command line application.

## How to build the application

IMPORTANT: Please make sure that file `mvnw` in root of project has execution permissions:
```shell script
chmod +x mvnw
```

To build the application, in command line please run:

```shell script
./mvnw clean package -U
```

This will build the application, package the artifacts and will also run unit and integration tests. 

## Running the application locally

In `marsrobot-rest/src/test/resources` there are resources to do requests to the application. You have the input files for the command line, but also a curl.txt with the curls for rest.  
Also mention, that the project contains 2 integration tests that run with the test data provided for the interview, matching the expected results also provided.
#### Testing purpose

###### Run Jars

For the purpose of testing the `deliverables` (jars) of this project, please follow this steps:

* In root directory, in command line please run:
```shell script
./mvnw clean package -U
```
* To run the rest application, write in command line:
```shell script
java -jar ABSOLUTE_PATH_TO_PROJECT/marsrobot/marsrobot-rest/target/marsrobot-rest-0.0.1-SNAPSHOT.jar
```
This will execute the spring boot rest application. You can now do requests to it. In `marsrobot-rest/src/test/resources/rest.examples` folder you can find a txt with curls to run the 2 test jsons provided.

* To run the command line application, in console:
```shell script
java -jar ABSOLUTE_PATH_TO_PROJECT/marsrobot/marsrobot-rest/target/marsrobot-rest-0.0.1-SNAPSHOT.jar ABSOLUTE_PATH_TO_INPUT_JSON ABSOLUTE_PATH_TO_OUTPUT_JSON
```
This will run the command line application, printing the steps in the logs and creating the file with the result. 

###### Run it using docker

IMPORTANT: Docker must be installed for this to work

The project has 2 different ways of generating docker images:

* Using Docker and Dockerfile

To  build the image using docker and Dockerfile, while being in root of the project, please run: 
```shell script
docker build .
```
The image will be built locally, and to run it, copy the generated id for the image:
```shell script
docker run -p 8080:8080 a0d3f4b027b3
```

* Using JIB plugin (Use of this plugin simplifies docker image builds and can be easily added to the pipeline to push directly to the docker registry without the need of having docker installed)
```shell script
./mvnw clean package -U jib:dockerBuild
```

This will generate an image with the name `marsrobot`. To run it:
```shell script
docker run -p 8080:8080 marsrobot
```

#### Other ways to run the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` methods of rest application in the `com.olympic.marsrobot.MarsrobotApplication` or of the command line application `com.olympic.marsrobot.MarsrobotCommandlineApplication` class from your IDE. If running the command line application, please configure your run configuration to pass the parameters.

- Download the zip or clone the Git repository.
- Open Command Prompt and Change directory (cd) to folder containing pom.xml
- Open Intellij 
   - File -> Open -> Navigate to the folder where the project is and select it
- Choose the Spring Boot Application main class and click play.

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

IMPORTANT: Before using the spring boot maven plugin, please in root directory run:

```shell script
./mvnw clean install -U -Dmaven.test.skip=true
```

Then, to run rest application, in `marsrobot-rest` folder, run: 
```shell script
./mvnw spring-boot:run
```

## Rest URL
|  URL                              |  Method  |
|-----------------------------------|----------|
|`http://localhost:8080/robot/run`  |   `Post` |

Body:
```json
{ 
  "terrain": [ 
    ["Fe", "Fe", "Se"], 
    ["W", "Si", "Obs"], 
    ["W", "Obs", "Zn"] 
  ], 
  "battery": 50, 
  "commands": [ 
    "F", "S", "R", "F", "S", "R", "F", "L", "F", "S" 
    ], 
  "initialPosition": { 
    "location" : { 
      "x" : 0, 
      "y" : 0 
    }, 
    "facing" : "East" 
  } 
}
```
Response:
```json
{
    "VisitedCells": [
        {
            "X": 0,
            "Y": 0
        },
        {
            "X": 1,
            "Y": 0
        },
        {
            "X": 1,
            "Y": 1
        },
        {
            "X": 0,
            "Y": 1
        },
        {
            "X": 0,
            "Y": 2
        }
    ],
    "SamplesCollected": [
        "Fe",
        "Si",
        "W"
    ],
    "Battery": 8,
    "FinalPosition": {
        "Location": {
            "X": 0,
            "Y": 2
        },
        "Facing": "South"
    }
}
```

### Actuator

To monitor and manage your application

|  URL |  Method |
|----------|--------------|
|`http://localhost:8080`  						| GET |
|`http://localhost:8080/actuator/`              | GET |
|`http://localhost:8080/actuator/health`    	| GET |
|`http://localhost:8080/actuator/info`      	| GET |

## packages

- `models` — to hold our entities and dtos;
- `services` — to hold our business logic;
- `robot` — robot implementation;
- `rest` — controllers;

- `resources/application.yaml` - Contains robot fall back strategies

- `test/` - contains unit and integration tests

## Final Notes
* The log lines of the application are more oriented to help in the review and not as production application. I have only used INFO and ERROR log levels and they are very verbosed. In a production application the log usage would be different.
* Some code that could be done in less lines than what it has. For example:
```java
...
if (command.isTakeAndStore()) {
    final TerrainContent terrainContent = terrain.get(position.getLocation().getY()).get(position.getLocation().getX());
    log.info("Taking sample {}", terrainContent);
    samplesCollected.add(terrainContent);
...
}
```

It is done this way for having this log detail for reviewing it.

* I haven't made use of code analysis tools like sonar or checkstyle for the reduced size of the project.

* Business logic is tested deeply with Unit and Integration tests. 

* There is a CI pipeline that is triggered with each commit that checks that build and tests

