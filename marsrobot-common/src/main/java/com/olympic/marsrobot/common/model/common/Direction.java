package com.olympic.marsrobot.common.model.common;

import lombok.Getter;
import lombok.ToString;
import lombok.extern.log4j.Log4j2;

import java.util.Arrays;

@Log4j2
@ToString
public enum Direction {
    North(90),
    South(270),
    East(0),
    West(180);

    public static final int DEGREES_360 = 360;
    @Getter
    private final int degrees;

    Direction(final int degrees) {
        this.degrees = degrees;
    }

    public static Direction getDirectionFromItsDegrees(final int degrees) {
        final int normalizedDegrees = degrees + DEGREES_360;
        return Arrays.stream(values()).filter(direction -> direction.degrees == normalizedDegrees % DEGREES_360).findFirst().get();
    }

}
