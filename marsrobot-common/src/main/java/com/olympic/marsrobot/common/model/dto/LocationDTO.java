package com.olympic.marsrobot.common.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.validation.constraints.Min;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class LocationDTO {
    @Min(0)
    @JsonProperty(value = "X")
    private Integer x;
    @Min(0)
    @JsonProperty(value = "Y")
    private Integer y;

    @Override
    public String toString() {
        return "\n  {\n" +
                "   x: " + x + ", \n" +
                "   y: " + y + "\n" +
                "  }";
    }
}
