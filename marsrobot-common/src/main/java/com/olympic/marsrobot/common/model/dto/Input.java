package com.olympic.marsrobot.common.model.dto;

import com.olympic.marsrobot.common.model.common.Command;
import com.olympic.marsrobot.common.model.common.Position;
import com.olympic.marsrobot.common.model.common.TerrainContent;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class Input {
    @NotEmpty(message = "Terrain is missing or empty")
    private List<List<TerrainContent>> terrain;
    @Positive(message = "Battery must be a positive number")
    @NotNull(message = "Battery is missing or empty")
    private Integer battery;
    @NotEmpty(message = "Command list must have at least one element")
    private List<Command> commands;
    @Valid
    private Position initialPosition;
}
