package com.olympic.marsrobot.common.model.common;

import lombok.Getter;

@Getter
public enum Command {
    F(3, 1, 0, false, 0),
    B(3, 1, 0, false, 0),
    L(2, 0, 90, false, 0),
    R(2, 0, -90, false, 0),
    S(8, 0, 0, true, 0),
    E(1, 0, 0, false, 10);

    private final int batteryConsumption;
    private final int moves;
    private final int turningDegree;
    private final boolean takeAndStore;
    private final int rechargeBattery;

    Command(final int batteryConsumption, final int moves, final int turningDegree, final boolean takeAndStore, final int rechargeBattery) {
        this.batteryConsumption = batteryConsumption;
        this.moves = moves;
        this.turningDegree = turningDegree;
        this.takeAndStore = takeAndStore;
        this.rechargeBattery = rechargeBattery;
    }

    @Override
    public String toString() {
        return this.name();
    }
}
