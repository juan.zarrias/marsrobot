package com.olympic.marsrobot.common.robot;

import com.olympic.marsrobot.common.exception.NoBackOffStrategyFoundException;
import com.olympic.marsrobot.common.model.common.Command;

public interface RobotActions {
    void applyMoveAndActions(Command command);
    boolean nextPositionIsInsideOfTerrainLimitsAndItIsNotAnObstacle();
    void calculateNextPosition(Command command);
    void applyBackOff() throws NoBackOffStrategyFoundException;
}
