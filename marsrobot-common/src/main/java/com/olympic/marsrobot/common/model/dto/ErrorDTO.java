package com.olympic.marsrobot.common.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@AllArgsConstructor
@Getter
@ToString
public class ErrorDTO {
    private final List<FieldErrorDTO> fieldErrors;

    public static ErrorDTO outOfTerrainErrorDTO(final int x, final int y) {
        return new ErrorDTO(List.of(new FieldErrorDTO("initialPosition.location", "Initial Position [" + x + "," + y + "] is out of terrain or has an obstacle on it")));
    }
}
