package com.olympic.marsrobot.common.model.common;

import lombok.Getter;

@Getter
public enum TerrainContent {
    Fe("Ferrum", "A deposit of iron"),
    Se("Selenium", "A deposit of selenium"),
    W("Water", "A deposit that contains water"),
    Si("Silicon", "A deposit that contains silicon"),
    Zn("Zinc", "A deposit that contains zinc"),
    Obs("Obstacle", "An obstacle cell in which the robot can’t go");

    private final String name;
    private final String description;

    TerrainContent(final String name, final String description) {
        this.name = name;
        this.description = description;
    }
}
