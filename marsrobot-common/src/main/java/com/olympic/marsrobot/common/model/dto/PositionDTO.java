package com.olympic.marsrobot.common.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.olympic.marsrobot.common.model.common.Direction;
import lombok.*;

import javax.validation.Valid;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class PositionDTO {
    @JsonProperty(value = "Location")
    @Valid
    private LocationDTO location;
    @JsonProperty(value = "Facing")
    private Direction facing;

    public static PositionDTO of(final LocationDTO location, final Direction facing) {
        return new PositionDTO(location, facing);
    }

    @Override
    public String toString() {
        return "\n {" +
                "   location: " + location + ",\n" +
                "   facing: " + facing.name() + "\n" +
                "  }";
    }
}
