package com.olympic.marsrobot.common.exception;

import com.olympic.marsrobot.common.robot.Robot;
import lombok.Getter;

public class NoBackOffStrategyFoundException extends RuntimeException {

    @Getter
    private final Robot robot;

    public NoBackOffStrategyFoundException(final Robot robot) {
        this.robot = robot;
    }
}
