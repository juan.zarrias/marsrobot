package com.olympic.marsrobot.common.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.olympic.marsrobot.common.model.common.TerrainContent;
import com.olympic.marsrobot.common.robot.Robot;
import lombok.*;

import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class Output {
    @JsonProperty(value = "VisitedCells")
    private List<LocationDTO> visitedCells;
    @JsonProperty(value = "SamplesCollected")
    private List<TerrainContent> samplesCollected;
    @JsonProperty(value = "Battery")
    private Integer battery;
    @JsonProperty(value = "FinalPosition")
    private PositionDTO finalPosition;

    public static Output fromRobot(final Robot robot) {
        return new Output(robot.getVisitedCells().stream().map(c -> new LocationDTO(c.getX(), c.getY())).collect(Collectors.toList()),
                robot.getSamplesCollected(),
                robot.getBattery(),
                PositionDTO.of(new LocationDTO(robot.getPosition().getLocation().getX(), robot.getPosition().getLocation().getY()), robot.getPosition().getFacing()));
    }

    @Override
    public String toString() {
        return "{" +
                "\n visitedCells: " + visitedCells + "" +
                ",\n samplesCollected: " + samplesCollected + "" +
                ",\n battery: " + battery +
                ",\n finalPosition: {" + finalPosition + "}" +
                "}";
    }
}
