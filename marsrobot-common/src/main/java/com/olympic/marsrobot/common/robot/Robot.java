package com.olympic.marsrobot.common.robot;

import com.olympic.marsrobot.common.exception.NoBackOffStrategyFoundException;
import com.olympic.marsrobot.common.model.common.*;
import com.olympic.marsrobot.common.model.dto.Input;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Builder
@Getter
@Log4j2
public class Robot implements RobotActions {
    private final List<List<TerrainContent>> terrain;
    private final List<Command> commands;
    private int battery;
    private Position position;
    private Position nextPosition;
    private final List<Location> visitedCells;
    private final List<TerrainContent> samplesCollected;
    private final List<List<Command>> fallBackStrategy;

    public static Robot createRobotFromInput(final Input input, final List<List<Command>> fallBackStrategy) {
        return Robot.builder()
                .terrain(input.getTerrain())
                .battery(input.getBattery())
                .commands(input.getCommands())
                .position(input.getInitialPosition())
                .nextPosition(input.getInitialPosition())
                .visitedCells(new ArrayList<>())
                .samplesCollected(new ArrayList<>())
                .fallBackStrategy(fallBackStrategy)
                .build();
    }

    @Override
    public void calculateNextPosition(final Command command) {
        final boolean isForward = command == Command.F;
        switch (position.getFacing()) {
            case East:
                nextPosition = new Position(new Location(position.getLocation().getX() + (isForward ? command.getMoves() : -1 * command.getMoves()), position.getLocation().getY()), Direction.East);
                break;
            case West:
                nextPosition = new Position(new Location(position.getLocation().getX() + (isForward ? command.getMoves() * -1 : command.getMoves()), position.getLocation().getY()), Direction.West);
                break;
            case North:
                nextPosition = new Position(new Location(position.getLocation().getX(), position.getLocation().getY() + (isForward ? command.getMoves() * -1 : command.getMoves())), Direction.North);
                break;
            case South:
                nextPosition = new Position(new Location(position.getLocation().getX(), position.getLocation().getY() + (isForward ? command.getMoves() : command.getMoves() * -1)), Direction.South);
                break;
            default:
                break;
        }
    }

    @Override
    public boolean nextPositionIsInsideOfTerrainLimitsAndItIsNotAnObstacle() {
        final var isInsideAndNotAnObstacle =
                nextPosition.getLocation().getX() > -1
                        && nextPosition.getLocation().getY() < terrain.size()
                        && nextPosition.getLocation().getY() > -1
                        && nextPosition.getLocation().getX() < terrain.get(0).size()
                        && !terrain.get(nextPosition.getLocation().getX()).get(nextPosition.getLocation().getY()).equals(TerrainContent.Obs);
        if (!isInsideAndNotAnObstacle) {
            log.info("Next position to move [{},{}] is OUT of terrain limits or has an obstacle", nextPosition.getLocation().getX(), nextPosition.getLocation().getY());
        }
        return isInsideAndNotAnObstacle;
    }

    @Override
    public void applyMoveAndActions(final Command command) {
        if (command == Command.F || command == Command.B) {
            move();
        }
        applyCommandActions(command);
    }

    @Override
    public void applyBackOff() throws NoBackOffStrategyFoundException {
        log.info("Applying Back Off Strategies");
        final Optional<List<Command>> successfulStrategyApplied = fallBackStrategy.stream()
                .filter(this::applyStrategy)
                .findFirst();
        if (successfulStrategyApplied.isEmpty()) {
            throw new NoBackOffStrategyFoundException(this);
        }

    }

    private boolean applyStrategy(final List<Command> strategy) {
        log.info("Applying Back Off Strategy [{}]", strategy);
        for (final Command command : strategy) {
            log.info("Executting command [{}]", command);
            calculateNextPosition(command);
            if (nextPositionIsInsideOfTerrainLimitsAndItIsNotAnObstacle()) {
                applyMoveAndActions(command);
            } else {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }

    private void applyCommandActions(final Command command) {
        log.info("Applying actions of command [{}]", command);
        battery -= command.getBatteryConsumption();
        battery += command.getRechargeBattery();
        position.setFacing(Direction.getDirectionFromItsDegrees(position.getFacing().getDegrees() + command.getTurningDegree()));
        if (command.isTakeAndStore()) {
            final TerrainContent terrainContent = terrain.get(position.getLocation().getY()).get(position.getLocation().getX());
            log.info("Taking sample {}", terrainContent);
            samplesCollected.add(terrainContent);
        }
    }


    private void move() {
        log.info("Robot moving to [{},{}]", nextPosition.getLocation().getX(), nextPosition.getLocation().getY());
        visitedCells.add(new Location(nextPosition.getLocation().getX(), nextPosition.getLocation().getY()));
        position.setLocation(new Location(nextPosition.getLocation().getX(), nextPosition.getLocation().getY()));
    }


}
