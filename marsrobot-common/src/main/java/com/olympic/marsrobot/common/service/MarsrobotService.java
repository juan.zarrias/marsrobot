package com.olympic.marsrobot.common.service;

import com.olympic.marsrobot.common.model.dto.Input;
import com.olympic.marsrobot.common.model.dto.Output;

import java.util.Optional;

@FunctionalInterface
public interface MarsrobotService {
    Optional<Output> runRobot(Input input);
}
