package com.olympic.marsrobot.common.model.common;

import lombok.*;

import javax.validation.constraints.Min;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class Location {
    @Min(value = 0, message = "X coordinate must be positive")
    private Integer x;
    @Min(value = 0, message = "Y coordinate must be positive")
    private Integer y;
}
