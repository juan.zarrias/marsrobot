package com.olympic.marsrobot.common.service;

import com.olympic.marsrobot.common.exception.NoBackOffStrategyFoundException;
import com.olympic.marsrobot.common.model.common.Command;
import com.olympic.marsrobot.common.model.common.Location;
import com.olympic.marsrobot.common.model.dto.Input;
import com.olympic.marsrobot.common.model.dto.Output;
import com.olympic.marsrobot.common.robot.Robot;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Log4j2
public class MarsrobotServiceImpl implements MarsrobotService {
    public static final String INITIAL_POSITION_IS_OUT_OF_TERRAIN_LIMITS_OR_HAS_AN_OBSTACLE = "Initial Position [{},{}] is OUT of terrain limits or has an obstacle";
    private final List<List<Command>> fallBackStrategy;

    public MarsrobotServiceImpl(@Value("${robot.fallback.strategy}") final String fallBackStrategy) {
        this.fallBackStrategy = Arrays
                .stream(fallBackStrategy.split("-"))
                .map(s -> List.of(s.split(",")))
                .map(x -> x.stream().map(Command::valueOf).collect(Collectors.toList()))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Output> runRobot(final Input input) {
        final Robot robot = Robot.createRobotFromInput(input, fallBackStrategy);
        if (robot.nextPositionIsInsideOfTerrainLimitsAndItIsNotAnObstacle()) {
            log.info("Initial Position [{},{}] is INSIDE of terrain limits and it is not an obstacle", input.getInitialPosition().getLocation().getX(), input.getInitialPosition().getLocation().getY());
            robot.getVisitedCells().add(new Location(robot.getPosition().getLocation().getX(), robot.getPosition().getLocation().getY()));
            return Optional.of(processCommands(robot));
        } else {
            //this validates that the initial position is not out ranges (positive ranges)
            //this validations is done manually. The rest are done via @valid in the controller
            log.info(INITIAL_POSITION_IS_OUT_OF_TERRAIN_LIMITS_OR_HAS_AN_OBSTACLE, input.getInitialPosition().getLocation().getX(), input.getInitialPosition().getLocation().getY());
            return Optional.empty();
        }
    }

    private Output processCommands(final Robot robot) throws NoBackOffStrategyFoundException{
        robot.getCommands().stream()
                .takeWhile(command -> robot.getBattery() >= command.getBatteryConsumption())
                .forEach(command -> {
                    robot.calculateNextPosition(command);
                    if (robot.nextPositionIsInsideOfTerrainLimitsAndItIsNotAnObstacle()) {
                        robot.applyMoveAndActions(command);
                    } else {
                        try {
                            robot.applyBackOff();
                        } catch (final NoBackOffStrategyFoundException e) {
                            log.info("No Back Off Strategy applied successfully, shutting down robot...");
                            throw e;
                        }

                    }
                });
        return Output.fromRobot(robot);
    }
}