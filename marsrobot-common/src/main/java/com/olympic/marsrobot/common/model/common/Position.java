package com.olympic.marsrobot.common.model.common;

import lombok.*;

import javax.validation.Valid;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Position {
    @Valid
    private Location location;
    private Direction facing;

    public static Position of(final Location location, final Direction facing) {
        return new Position(location, facing);
    }
}
