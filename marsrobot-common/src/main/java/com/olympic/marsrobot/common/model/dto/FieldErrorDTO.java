package com.olympic.marsrobot.common.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class FieldErrorDTO {
    private final String field;
    private final String message;
}
