package com.olympic.marsrobot.common.service;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.olympic.marsrobot.common.model.dto.Input;
import com.olympic.marsrobot.common.model.dto.Output;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class MarsrobotServiceUT {

    @Test
    void shouldRunRobotHappyPathOne() throws URISyntaxException, IOException {
        //given:
        MarsrobotService marsrobotService = new MarsrobotServiceImpl("E,R,F-E,L,F-E,L,L,F-E,B,R,F-E,B,B,L,F-E,F,F-E,F,L,F,L,F");

        //and:
        final String exampleInput = Files.readString(Paths.get(getClass().getResource("/input/test_run_1.json").toURI()));
        final Input input = new ObjectMapper().readValue(exampleInput, Input.class);

        //and:
        final String exampleOutput = Files.readString(Paths.get(getClass().getResource("/output/test_sol_1.json").toURI()));
        final Output expectedOutput = new ObjectMapper().readValue(exampleOutput, Output.class);

        //when:
        final Optional<Output> outputOpt = marsrobotService.runRobot(input);

        //then:
        assertThat(outputOpt.isPresent(), is(Boolean.TRUE));
        final Output output = outputOpt.get();
        assertThat(output.getBattery(), is(expectedOutput.getBattery()));
        assertThat(output.getFinalPosition(), is(expectedOutput.getFinalPosition()));
        assertThat(output.getVisitedCells().size(), is(expectedOutput.getVisitedCells().size()));
        assertThat(output.getVisitedCells(), is(expectedOutput.getVisitedCells()));
        assertThat(output.getSamplesCollected().size(), is(expectedOutput.getSamplesCollected().size()));
        assertThat(output.getSamplesCollected(), is(expectedOutput.getSamplesCollected()));
    }

    @Test
    void shouldRunRobotHappyPathTwo() throws URISyntaxException, IOException {
        //given:
        MarsrobotService marsrobotService = new MarsrobotServiceImpl("E,R,F-E,L,F-E,L,L,F-E,B,R,F-E,B,B,L,F-E,F,F-E,F,L,F,L,F");

        //and:
        final String exampleInput = Files.readString(Paths.get(getClass().getResource("/input/test_run_2.json").toURI()));
        final Input input = new ObjectMapper().readValue(exampleInput, Input.class);

        //and:
        final String exampleOutput = Files.readString(Paths.get(getClass().getResource("/output/test_sol_2.json").toURI()));
        final Output expectedOutput = new ObjectMapper().readValue(exampleOutput, Output.class);

        //when:
        final Optional<Output> outputOpt = marsrobotService.runRobot(input);

        //then:
        assertThat(outputOpt.isPresent(), is(Boolean.TRUE));
        final Output output = outputOpt.get();
        assertThat(output.getBattery(), is(expectedOutput.getBattery()));
        assertThat(output.getFinalPosition(), is(expectedOutput.getFinalPosition()));
        assertThat(output.getVisitedCells().size(), is(expectedOutput.getVisitedCells().size()));
        assertThat(output.getVisitedCells(), is(expectedOutput.getVisitedCells()));
        assertThat(output.getSamplesCollected().size(), is(expectedOutput.getSamplesCollected().size()));
        assertThat(output.getSamplesCollected(), is(expectedOutput.getSamplesCollected()));
    }
}
