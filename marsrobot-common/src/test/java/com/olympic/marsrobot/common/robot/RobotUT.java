package com.olympic.marsrobot.common.robot;

import com.olympic.marsrobot.common.exception.NoBackOffStrategyFoundException;
import com.olympic.marsrobot.common.model.common.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@ExtendWith(MockitoExtension.class)
public class RobotUT {
    public static final int INITIAL_BATTERY = 20;
    public static final List<List<TerrainContent>> TERRAIN = List.of(
            List.of(TerrainContent.Fe, TerrainContent.Fe, TerrainContent.Se),
            List.of(TerrainContent.W, TerrainContent.Si, TerrainContent.Obs),
            List.of(TerrainContent.W, TerrainContent.Obs, TerrainContent.Zn));
    public static final List<List<Command>> ONE_FALL_BACK_STRATEGY = List.of(List.of(Command.valueOf("E"), Command.valueOf("R"), Command.valueOf("F")));
    public static final List<List<Command>> OTHER_ONE_FALL_BACK_STRATEGY = List.of(List.of(Command.valueOf("E"), Command.valueOf("F"), Command.valueOf("L"), Command.valueOf("F"), Command.valueOf("L"), Command.valueOf("F")));
    public static final List<List<Command>> COMPLETE_FALL_BACK_STRATEGY = List.of(
            List.of(Command.valueOf("E"), Command.valueOf("R"), Command.valueOf("F")),
            List.of(Command.valueOf("E"), Command.valueOf("L"), Command.valueOf("F")),
            List.of(Command.valueOf("E"), Command.valueOf("L"), Command.valueOf("L"), Command.valueOf("F")),
            List.of(Command.valueOf("E"), Command.valueOf("B"), Command.valueOf("R"), Command.valueOf("F")),
            List.of(Command.valueOf("E"), Command.valueOf("B"), Command.valueOf("B"), Command.valueOf("L"), Command.valueOf("F")),
            List.of(Command.valueOf("E"), Command.valueOf("F"), Command.valueOf("F")),
            List.of(Command.valueOf("E"), Command.valueOf("F"), Command.valueOf("L"), Command.valueOf("F"), Command.valueOf("L"), Command.valueOf("F")));
    public static final List<List<Command>> OTHER_COMPLETE_FALL_BACK_STRATEGY = List.of(
            List.of(Command.valueOf("E"), Command.valueOf("L"), Command.valueOf("F")),
            List.of(Command.valueOf("E"), Command.valueOf("R"), Command.valueOf("R"), Command.valueOf("F")),
            List.of(Command.valueOf("E"), Command.valueOf("B"), Command.valueOf("R"), Command.valueOf("F")),
            List.of(Command.valueOf("E"), Command.valueOf("B"), Command.valueOf("B"), Command.valueOf("L"), Command.valueOf("F")),
            List.of(Command.valueOf("E"), Command.valueOf("F"), Command.valueOf("F")),
            List.of(Command.valueOf("E"), Command.valueOf("F"), Command.valueOf("L"), Command.valueOf("F"), Command.valueOf("L"), Command.valueOf("F")));


    //Test Subject:
    //Robot robot

    @Test
    void shouldCalculateNextPositionProperlyWhenMovingForwardFacingEast() {
        //given:
        final Robot robot = new Robot(TERRAIN, List.of(Command.E, Command.F), INITIAL_BATTERY, Position.of(new Location(0, 0), Direction.East), Position.of(new Location(0, 0), Direction.East), new ArrayList<>(), new ArrayList<>(), null);

        //and:
        final Position expectedNextPosition = Position.of(new Location(1, 0), Direction.East);

        //when:
        robot.calculateNextPosition(Command.F);

        //then:
        assertThat(robot.getNextPosition().getLocation(), is(expectedNextPosition.getLocation()));
        assertThat(robot.getNextPosition().getFacing(), is(expectedNextPosition.getFacing()));
    }

    @Test
    void shouldCalculateNextPositionProperlyWhenMovingBackwardsFacingEast() {
        //given:
        final Robot robot = new Robot(TERRAIN, List.of(Command.E, Command.F), INITIAL_BATTERY, Position.of(new Location(0, 0), Direction.East), Position.of(new Location(0, 0), Direction.East), new ArrayList<>(), new ArrayList<>(), null);

        //and:
        final Position expectedNextPosition = Position.of(new Location(-1, 0), Direction.East);

        //when:
        robot.calculateNextPosition(Command.B);

        //then:
        assertThat(robot.getNextPosition().getLocation(), is(expectedNextPosition.getLocation()));
        assertThat(robot.getNextPosition().getFacing(), is(expectedNextPosition.getFacing()));
    }

    @Test
    void shouldCalculateNextPositionProperlyWhenMovingForwardFacingWest() {
        //given:
        final Robot robot = new Robot(TERRAIN, List.of(Command.E, Command.F), INITIAL_BATTERY, Position.of(new Location(0, 0), Direction.West), Position.of(new Location(0, 0), Direction.West), new ArrayList<>(), new ArrayList<>(), null);

        //and:
        final Position expectedNextPosition = Position.of(new Location(-1, 0), Direction.West);


        //when:
        robot.calculateNextPosition(Command.F);

        //then:
        assertThat(robot.getNextPosition().getLocation(), is(expectedNextPosition.getLocation()));
        assertThat(robot.getNextPosition().getFacing(), is(expectedNextPosition.getFacing()));
    }

    @Test
    void shouldCalculateNextPositionProperlyWhenMovingBackwardsFacingWest() {
        //given:
        final Robot robot = new Robot(TERRAIN, List.of(Command.E, Command.F), INITIAL_BATTERY, Position.of(new Location(0, 0), Direction.West), Position.of(new Location(0, 0), Direction.West), new ArrayList<>(), new ArrayList<>(), null);

        //and:
        final Position expectedNextPosition = Position.of(new Location(1, 0), Direction.West);

        //when:
        robot.calculateNextPosition(Command.B);

        //then:
        assertThat(robot.getNextPosition().getLocation(), is(expectedNextPosition.getLocation()));
        assertThat(robot.getNextPosition().getFacing(), is(expectedNextPosition.getFacing()));
    }

    @Test
    void shouldCalculateNextPositionProperlyWhenMovingForwardFacingNorth() {
        //given:
        final Robot robot = new Robot(TERRAIN, List.of(Command.E, Command.F), INITIAL_BATTERY, Position.of(new Location(0, 0), Direction.North), Position.of(new Location(0, 0), Direction.North), new ArrayList<>(), new ArrayList<>(), null);

        //and:
        final Position expectedNextPosition = Position.of(new Location(0, -1), Direction.North);

        //when:
        robot.calculateNextPosition(Command.F);

        //then:
        assertThat(robot.getNextPosition().getLocation(), is(expectedNextPosition.getLocation()));
        assertThat(robot.getNextPosition().getFacing(), is(expectedNextPosition.getFacing()));
    }

    @Test
    void shouldCalculateNextPositionProperlyWhenMovingBackwardsFacingNorth() {
        //given:
        final Robot robot = new Robot(TERRAIN, List.of(Command.E, Command.F), INITIAL_BATTERY, Position.of(new Location(0, 0), Direction.North), Position.of(new Location(0, 0), Direction.North), new ArrayList<>(), new ArrayList<>(), null);

        //and:
        final Position expectedNextPosition = Position.of(new Location(0, 1), Direction.North);

        //when:
        robot.calculateNextPosition(Command.B);

        //then:
        assertThat(robot.getNextPosition().getLocation(), is(expectedNextPosition.getLocation()));
        assertThat(robot.getNextPosition().getFacing(), is(expectedNextPosition.getFacing()));
    }

    @Test
    void shouldCalculateNextPositionProperlyWhenMovingForwardFacingSouth() {
        //given:
        final Robot robot = new Robot(TERRAIN, List.of(Command.E, Command.F), INITIAL_BATTERY, Position.of(new Location(0, 0), Direction.South), Position.of(new Location(0, 0), Direction.South), new ArrayList<>(), new ArrayList<>(), null);

        //and:
        final Position expectedNextPosition = Position.of(new Location(0, 1), Direction.South);

        //when:
        robot.calculateNextPosition(Command.F);

        //then:
        assertThat(robot.getNextPosition().getLocation(), is(expectedNextPosition.getLocation()));
        assertThat(robot.getNextPosition().getFacing(), is(expectedNextPosition.getFacing()));
    }

    @Test
    void shouldCalculateNextPositionProperlyWhenMovingBackwardsFacingSouth() {
        //given:
        final Robot robot = new Robot(TERRAIN, List.of(Command.E, Command.F), INITIAL_BATTERY, Position.of(new Location(0, 0), Direction.South), Position.of(new Location(0, 0), Direction.South), new ArrayList<>(), new ArrayList<>(), null);

        //and:
        final Position expectedNextPosition = Position.of(new Location(0, -1), Direction.South);

        //when:
        robot.calculateNextPosition(Command.B);

        //then:
        assertThat(robot.getNextPosition().getLocation(), is(expectedNextPosition.getLocation()));
        assertThat(robot.getNextPosition().getFacing(), is(expectedNextPosition.getFacing()));
    }

    @ParameterizedTest
    @ValueSource(strings = {"0,0", "0,1", "0,2", "1,0", "1,1", "2,0", "2,2"})
    void shouldNotFindObstacleNorOutOfTerrainInLocations(final String parametrizedValue) {
        //given:
        final Position nextPosition = Position.of(new Location(Integer.parseInt(parametrizedValue.split(",")[0]), Integer.parseInt(parametrizedValue.split(",")[1])), Direction.South);

        //and:
        final Robot robot = new Robot(TERRAIN, List.of(Command.E, Command.F), INITIAL_BATTERY, Position.of(new Location(0, 0), Direction.South), nextPosition, new ArrayList<>(), new ArrayList<>(), null);

        //expect:
        assertThat(robot.nextPositionIsInsideOfTerrainLimitsAndItIsNotAnObstacle(), is(Boolean.TRUE));
    }

    @ParameterizedTest
    @ValueSource(strings = {"1,2", "2,1", "-1,0", "-1,2", "3,0", "3,1", "0,-1", "2,-1", "1,3", "3,3", "25,12", "-200,10", "-3000,1"})
    void shouldFindOutItIsOutOfTerrainOrAnObstacleInLocations(final String parametrizedValue) {
        //given:
        final Position nextPosition = Position.of(new Location(Integer.parseInt(parametrizedValue.split(",")[0]), Integer.parseInt(parametrizedValue.split(",")[1])), Direction.South);

        //and:
        final Robot robot = new Robot(TERRAIN, List.of(Command.E, Command.F), INITIAL_BATTERY, Position.of(new Location(0, 0), Direction.South), nextPosition, new ArrayList<>(), new ArrayList<>(), null);

        //expect:
        assertThat(robot.nextPositionIsInsideOfTerrainLimitsAndItIsNotAnObstacle(), is(Boolean.FALSE));
    }

    @ParameterizedTest
    @ValueSource(strings = {"F-2,0-East", "B-0,0-East", "L-1,0-North", "R-1,0-South", "S-1,0-East", "E-1,0-East"})
    void shouldMoveToNextPositionOnlyIfMovingForwardOrBackwardsAndTurnIfHasToFromCoordinates1x0FacingEast(final String parametrizedValue) {
        //given:
        final Command command = Command.valueOf(parametrizedValue.split("-")[0]);

        //and:
        final String coordinates = parametrizedValue.split("-")[1];
        final Direction expectedDirection = Direction.valueOf(parametrizedValue.split("-")[2]);
        final Position expectedNextPosition = Position.of(new Location(Integer.parseInt(coordinates.split(",")[0]), Integer.parseInt(coordinates.split(",")[1])), expectedDirection);

        //and:
        final Robot robot = new Robot(TERRAIN, List.of(Command.E, Command.F), INITIAL_BATTERY, Position.of(new Location(1, 0), Direction.East), expectedNextPosition, new ArrayList<>(), new ArrayList<>(), null);

        //when:
        robot.applyMoveAndActions(command);

        //then:
        assertThat(robot.getPosition(), is(expectedNextPosition));
        assertThat(robot.getBattery(), is(INITIAL_BATTERY - command.getBatteryConsumption() + command.getRechargeBattery()));
        if (command == Command.F || command == Command.B) {
            assertThat(robot.getVisitedCells().get(robot.getVisitedCells().size() - 1), is(expectedNextPosition.getLocation()));
        }
    }

    @ParameterizedTest
    @ValueSource(strings = {"F-0,0-West", "B-2,0-West", "L-1,0-South", "R-1,0-North", "S-1,0-West", "E-1,0-West"})
    void shouldMoveToNextPositionOnlyIfMovingForwardOrBackwardsAndTurnIfHasToFromCoordinates1x0FacingWest(final String parametrizedValue) {
        //given:
        final Command command = Command.valueOf(parametrizedValue.split("-")[0]);

        //and:
        final String coordinates = parametrizedValue.split("-")[1];
        final Direction expectedDirection = Direction.valueOf(parametrizedValue.split("-")[2]);
        final Position expectedNextPosition = Position.of(new Location(Integer.parseInt(coordinates.split(",")[0]), Integer.parseInt(coordinates.split(",")[1])), expectedDirection);

        //and:
        final Robot robot = new Robot(TERRAIN, List.of(Command.E, Command.F), INITIAL_BATTERY, Position.of(new Location(1, 0), Direction.West), expectedNextPosition, new ArrayList<>(), new ArrayList<>(), null);

        //when:
        robot.applyMoveAndActions(command);

        //then:
        assertThat(robot.getPosition(), is(expectedNextPosition));
        assertThat(robot.getBattery(), is(INITIAL_BATTERY - command.getBatteryConsumption() + command.getRechargeBattery()));
        if (command == Command.F || command == Command.B) {
            assertThat(robot.getVisitedCells().get(robot.getVisitedCells().size() - 1), is(expectedNextPosition.getLocation()));
        }
    }

    @ParameterizedTest
    @ValueSource(strings = {"F-0,0-North", "B-0,2-North", "L-0,1-West", "R-0,1-East", "S-0,1-North", "E-0,1-North"})
    void shouldMoveToNextPositionOnlyIfMovingForwardOrBackwardsAndTurnIfHasToFromCoordinates0x1FacingNorth(final String parametrizedValue) {
        //given:
        final Command command = Command.valueOf(parametrizedValue.split("-")[0]);

        //and:
        final String coordinates = parametrizedValue.split("-")[1];
        final Direction expectedDirection = Direction.valueOf(parametrizedValue.split("-")[2]);
        final Position expectedNextPosition = Position.of(new Location(Integer.parseInt(coordinates.split(",")[0]), Integer.parseInt(coordinates.split(",")[1])), expectedDirection);

        //and:
        final Robot robot = new Robot(TERRAIN, List.of(Command.E, Command.F), INITIAL_BATTERY, Position.of(new Location(0, 1), Direction.North), expectedNextPosition, new ArrayList<>(), new ArrayList<>(), null);

        //when:
        robot.applyMoveAndActions(command);

        //then:
        assertThat(robot.getPosition(), is(expectedNextPosition));
        assertThat(robot.getBattery(), is(INITIAL_BATTERY - command.getBatteryConsumption() + command.getRechargeBattery()));
        if (command == Command.F || command == Command.B) {
            assertThat(robot.getVisitedCells().get(robot.getVisitedCells().size() - 1), is(expectedNextPosition.getLocation()));
        }
    }

    @ParameterizedTest
    @ValueSource(strings = {"F-0,2-South", "B-0,0-South", "L-0,1-East", "R-0,1-West", "S-0,1-South", "E-0,1-South"})
    void shouldMoveToNextPositionOnlyIfMovingForwardOrBackwardsAndTurnIfHasToFromCoordinates0x1FacingSouth(final String parametrizedValue) {
        //given:
        final Command command = Command.valueOf(parametrizedValue.split("-")[0]);

        //and:
        final String coordinates = parametrizedValue.split("-")[1];
        final Direction expectedDirection = Direction.valueOf(parametrizedValue.split("-")[2]);
        final Position expectedNextPosition = Position.of(new Location(Integer.parseInt(coordinates.split(",")[0]), Integer.parseInt(coordinates.split(",")[1])), expectedDirection);

        //and:
        final Robot robot = new Robot(TERRAIN, List.of(Command.E, Command.F), INITIAL_BATTERY, Position.of(new Location(0, 1), Direction.South), expectedNextPosition, new ArrayList<>(), new ArrayList<>(), null);

        //when:
        robot.applyMoveAndActions(command);

        //then:
        assertThat(robot.getPosition(), is(expectedNextPosition));
        assertThat(robot.getBattery(), is(INITIAL_BATTERY - command.getBatteryConsumption() + command.getRechargeBattery()));
        if (command == Command.F || command == Command.B) {
            assertThat(robot.getVisitedCells().get(robot.getVisitedCells().size() - 1), is(expectedNextPosition.getLocation()));
        }
    }

    @ParameterizedTest
    @ValueSource(strings = {"0,2-W", "0,0-Fe", "0,1-W", "1,1-Si", "1,1-Si"})
    void shouldTakeSampleCorrectlyFromCoordinates(final String parametrizedValue) {
        //given:
        final String coordinates = parametrizedValue.split("-")[0];
        final Position position = Position.of(new Location(Integer.parseInt(coordinates.split(",")[0]), Integer.parseInt(coordinates.split(",")[1])), Direction.South);

        //and:
        final TerrainContent expectedSampleCollected = TerrainContent.valueOf(parametrizedValue.split("-")[1]);

        //and:
        final Robot robot = new Robot(TERRAIN, List.of(Command.E, Command.F), INITIAL_BATTERY, position, position, new ArrayList<>(), new ArrayList<>(), null);

        //when:
        robot.applyMoveAndActions(Command.S);

        //then:
        assertThat(robot.getPosition(), is(position));
        assertThat(robot.getSamplesCollected(), hasSize(1));
        assertThat(robot.getSamplesCollected().get(0), is(expectedSampleCollected));
    }

//    void applyFallback();

    @Test
    void shouldApplyFallBackSuccessfullyInFirstTryWithOneOnlyStrategy() {
        //given:
        final Robot robot = new Robot(TERRAIN, List.of(Command.E, Command.F), INITIAL_BATTERY, Position.of(new Location(1, 1), Direction.South), Position.of(new Location(1, 1), Direction.South), new ArrayList<>(), new ArrayList<>(), ONE_FALL_BACK_STRATEGY);

        //and:
        final Location expectedLocation = new Location(0, 1);
        final Position expectedFinalPosition = Position.of(expectedLocation, Direction.West);

        //when:
        robot.applyBackOff();

        //then:
        assertThat(robot.getPosition(), is(expectedFinalPosition));
        assertThat(robot.getBattery(), is(INITIAL_BATTERY - Command.E.getBatteryConsumption() - Command.R.getBatteryConsumption() - Command.F.getBatteryConsumption() + Command.E.getRechargeBattery()));
        assertThat(robot.getVisitedCells(), contains(expectedLocation));
        assertThat(robot.getVisitedCells(), hasSize(1));
    }

    @Test
    void shouldThrowExceptionIfStrategyIsNotSuccessful() {
        //given:
        final Robot robot = new Robot(TERRAIN, List.of(Command.E, Command.F), INITIAL_BATTERY, Position.of(new Location(2, 2), Direction.South), Position.of(new Location(2, 2), Direction.South), new ArrayList<>(), new ArrayList<>(), OTHER_ONE_FALL_BACK_STRATEGY);

        //and:
        final Location expectedLocation = new Location(2, 2);
        final Position expectedFinalPosition = Position.of(expectedLocation, Direction.South);

        try {
            //when:
            robot.applyBackOff();
        } catch (final NoBackOffStrategyFoundException e) {
            //then:
            assertThat(e.getClass(), is(NoBackOffStrategyFoundException.class));
            assertThat(e.getRobot().getPosition(), is(expectedFinalPosition));
        }
    }

    @Test
    void shouldThrowExceptionIfStrategyIsNotSuccessfulBecauseRobotIsBlocked() {
        //given:
        final Robot robot = new Robot(TERRAIN, List.of(Command.E, Command.F), INITIAL_BATTERY, Position.of(new Location(2, 2), Direction.South), Position.of(new Location(2, 2), Direction.South), new ArrayList<>(), new ArrayList<>(), COMPLETE_FALL_BACK_STRATEGY);

        //and:
        final Location expectedLocation = new Location(2, 2);
        final Position expectedFinalPosition = Position.of(expectedLocation, Direction.North);

        try {
            //when:
            robot.applyBackOff();
        } catch (final NoBackOffStrategyFoundException e) {
            //then:
            assertThat(e.getClass(), is(NoBackOffStrategyFoundException.class));
            assertThat(e.getRobot().getPosition(), is(expectedFinalPosition));
        }
    }

    @Test
    void shouldFindFirstStrategyToPassObstacle() {
        //given:
        final Robot robot = new Robot(TERRAIN, List.of(Command.E, Command.F), INITIAL_BATTERY, Position.of(new Location(2, 0), Direction.South), Position.of(new Location(2, 0), Direction.South), new ArrayList<>(), new ArrayList<>(), COMPLETE_FALL_BACK_STRATEGY);

        //and:
        final Location expectedLocation = new Location(1, 0);
        final Position expectedFinalPosition = Position.of(expectedLocation, Direction.West);

        //when:
        robot.applyBackOff();

        //then:
        assertThat(robot.getPosition(), is(expectedFinalPosition));
    }

    @Test
    void shouldFindNotFirstStrategyToPassObstacle() {
        //given:
        final Robot robot = new Robot(TERRAIN, List.of(Command.E, Command.F), INITIAL_BATTERY, Position.of(new Location(2, 0), Direction.South), Position.of(new Location(2, 0), Direction.South), new ArrayList<>(), new ArrayList<>(), OTHER_COMPLETE_FALL_BACK_STRATEGY);

        //and:
        final Location expectedLocation = new Location(1, 0);
        final Position expectedFinalPosition = Position.of(expectedLocation, Direction.West);


        //when:
        robot.applyBackOff();

        //then:
        assertThat(robot.getPosition(), is(expectedFinalPosition));
    }

}
