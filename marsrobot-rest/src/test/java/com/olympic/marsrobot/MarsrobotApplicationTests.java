package com.olympic.marsrobot;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.nio.file.Files;
import java.nio.file.Paths;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class MarsrobotApplicationTests {

    @Autowired
    protected WebApplicationContext context;

    protected MockMvc mvc;

    @BeforeEach
    void setUp() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .build();
    }

    @Test
    void contextLoads() {
    }

    @Test
    void givenExampleNumberOneHappyPath() throws Exception {
        //given:
        final String request = Files.readString(Paths.get(getClass().getResource("/input/test_run_1.json").toURI()));

        //and:
        final String expectedResponse = Files.readString(Paths.get(getClass().getResource("/output/test_sol_1.json").toURI()));

        //expect:
        mvc.perform(
                post("/robot/run")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().json(expectedResponse)
                );
    }

    @Test
    void givenExampleNumberTwoHappyPath() throws Exception {
        //given:
        final String request = Files.readString(Paths.get(getClass().getResource("/input/test_run_2.json").toURI()));

        //and:
        final String expectedResponse = Files.readString(Paths.get(getClass().getResource("/output/test_sol_2.json").toURI()));

        //expect:
        mvc.perform(
                post("/robot/run")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(expectedResponse)
                );
    }

    @Test
    void shouldReturnBadRequestIfBodyIsEmpty() throws Exception {
        //expect:
        mvc.perform(
                post("/robot/run")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldReturnBadRequestIfTerrainIsEmpty() throws Exception {
        //given:
        final String request = Files.readString(Paths.get(getClass().getResource("/bad-requests/empty_terrain.json").toURI()));

        //expect:
        mvc.perform(
                post("/robot/run")
                        .content(request)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().json("{\"fieldErrors\":[{\"field\":\"terrain\",\"message\":\"Terrain is missing or empty\"}]}"));
    }

    @Test
    void shouldReturnBadRequestIfTerrainIsMissing() throws Exception {
        //given:
        final String request = Files.readString(Paths.get(getClass().getResource("/bad-requests/missing_terrain.json").toURI()));

        //expect:
        mvc.perform(
                post("/robot/run")
                        .content(request)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().json("{\"fieldErrors\":[{\"field\":\"terrain\",\"message\":\"Terrain is missing or empty\"}]}"));
    }

    @Test
    void shouldReturnBadRequestIfBatteryIsZero() throws Exception {
        //given:
        final String request = Files.readString(Paths.get(getClass().getResource("/bad-requests/zero_battery.json").toURI()));

        //expect:
        mvc.perform(
                post("/robot/run")
                        .content(request)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().json("{\"fieldErrors\":[{\"field\":\"battery\",\"message\":\"Battery must be a positive number\"}]}"));
    }

    @Test
    void shouldReturnBadRequestIfBatteryIsNegative() throws Exception {
        //given:
        final String request = Files.readString(Paths.get(getClass().getResource("/bad-requests/zero_battery.json").toURI()));

        //expect:
        mvc.perform(
                post("/robot/run")
                        .content(request)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().json("{\"fieldErrors\":[{\"field\":\"battery\",\"message\":\"Battery must be a positive number\"}]}"));
    }

    @Test
    void shouldReturnBadRequestIfBatteryIsMissing() throws Exception {
        //given:
        final String request = Files.readString(Paths.get(getClass().getResource("/bad-requests/missing_battery.json").toURI()));

        //expect:
        mvc.perform(
                post("/robot/run")
                        .content(request)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().json("{\"fieldErrors\":[{\"field\":\"battery\",\"message\":\"Battery is missing or empty\"}]}"));
    }

    @Test
    void shouldReturnBadRequestIfCommandsIsEmpty() throws Exception {
        //given:
        final String request = Files.readString(Paths.get(getClass().getResource("/bad-requests/empty_commands.json").toURI()));

        //expect:
        mvc.perform(
                post("/robot/run")
                        .content(request)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().json("{\"fieldErrors\":[{\"field\":\"commands\",\"message\":\"Command list must have at least one element\"}]}"));
    }

    @Test
    void shouldReturnBadRequestIfCommandsIsMissing() throws Exception {
        //given:
        final String request = Files.readString(Paths.get(getClass().getResource("/bad-requests/missing_commands.json").toURI()));

        //expect:
        mvc.perform(
                post("/robot/run")
                        .content(request)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().json("{\"fieldErrors\":[{\"field\":\"commands\",\"message\":\"Command list must have at least one element\"}]}"));
    }

    @Test
    void shouldReturnBadRequestIfCommandsAreWrong() throws Exception {
        //given:
        final String request = Files.readString(Paths.get(getClass().getResource("/bad-requests/wrong_commands.json").toURI()));

        //expect:
        mvc.perform(
                post("/robot/run")
                        .content(request)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldReturnBadRequestIfInitialPositionXCoordinateIsNegative() throws Exception {
        //given:
        final String request = Files.readString(Paths.get(getClass().getResource("/bad-requests/initial_position_x_negative.json").toURI()));

        //expect:
        mvc.perform(
                post("/robot/run")
                        .content(request)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().json("{\"fieldErrors\":[{\"field\":\"initialPosition.location.x\",\"message\":\"X coordinate must be positive\"}]}"));
    }

    @Test
    void shouldReturnBadRequestIfInitialPositionYCoordinateIsNegative() throws Exception {
        //given:
        final String request = Files.readString(Paths.get(getClass().getResource("/bad-requests/initial_position_y_negative.json").toURI()));

        //expect:
        mvc.perform(
                post("/robot/run")
                        .content(request)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().json("{\"fieldErrors\":[{\"field\":\"initialPosition.location.y\",\"message\":\"Y coordinate must be positive\"}]}"));
    }

    @Test
    void shouldReturnBadRequestIfInitialPositionXCoordinateIsOutOfTerrain() throws Exception {
        //given:
        final String request = Files.readString(Paths.get(getClass().getResource("/bad-requests/initial_position_x_out.json").toURI()));

        //expect:
        mvc.perform(
                post("/robot/run")
                        .content(request)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().json("{\"fieldErrors\":[{\"field\":\"initialPosition.location\",\"message\":\"Initial Position [7,0] is out of terrain or has an obstacle on it\"}]}"));
    }

    @Test
    void shouldReturnBadRequestIfInitialPositionYCoordinateIsOutOfTerrain() throws Exception {
        //given:
        final String request = Files.readString(Paths.get(getClass().getResource("/bad-requests/initial_position_y_out.json").toURI()));

        //expect:
        mvc.perform(
                post("/robot/run")
                        .content(request)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().json("{\"fieldErrors\":[{\"field\":\"initialPosition.location\",\"message\":\"Initial Position [0,4] is out of terrain or has an obstacle on it\"}]}"));
    }

    @Test
    void shouldReturnBadRequestIfInitialPositionYCoordinateIsAnObstacle() throws Exception {
        //given:
        final String request = Files.readString(Paths.get(getClass().getResource("/bad-requests/initial_position_obstacle.json").toURI()));

        //expect:
        mvc.perform(
                post("/robot/run")
                        .content(request)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().json("{\"fieldErrors\":[{\"field\":\"initialPosition.location\",\"message\":\"Initial Position [2,1] is out of terrain or has an obstacle on it\"}]}"));
    }

    @Test
    void shouldReturnBadRequestIfFacingIsIncorrect() throws Exception {
        //given:
        final String request = Files.readString(Paths.get(getClass().getResource("/bad-requests/facing_incorrect.json").toURI()));

        //expect:
        mvc.perform(
                post("/robot/run")
                        .content(request)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    //initial_position_obstacle
}
