package com.olympic.marsrobot;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

@SpringBootApplication
@ComponentScan({"com.olympic.marsrobot.rest", "com.olympic.marsrobot.common.service"})
public class MarsrobotApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsrobotApplication.class, args);
    }

    @Bean
    public MappingJackson2HttpMessageConverter customJson(){
        return new MappingJackson2HttpMessageConverter(
                new Jackson2ObjectMapperBuilder()
                        .indentOutput(true)
                        .serializationInclusion(JsonInclude.Include.NON_NULL)
                        .build()
        );
    }

}
