package com.olympic.marsrobot.rest;

import com.olympic.marsrobot.common.model.dto.ErrorDTO;
import com.olympic.marsrobot.common.model.dto.FieldErrorDTO;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
@EnableWebMvc
public class MarsrobotExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            final MethodArgumentNotValidException ex,
            final HttpHeaders headers,
            final HttpStatus status,
            final WebRequest request) {
        return ResponseEntity.badRequest()
                .contentType(MediaType.APPLICATION_JSON)
                .body(new ErrorDTO(ex.getBindingResult()
                        .getFieldErrors()
                        .stream()
                        .map(v -> new FieldErrorDTO(v.getField(), v.getDefaultMessage()))
                        .collect(Collectors.toList())));
    }


    @ExceptionHandler({ConstraintViolationException.class})
    public ResponseEntity<Object> handleConstraintViolation(final ConstraintViolationException ex, final WebRequest request) {
        return ResponseEntity.badRequest()
                .body(new ErrorDTO(ex.getConstraintViolations()
                        .stream()
                        .map(v -> new FieldErrorDTO(v.getPropertyPath().toString(), v.getMessage()))
                        .collect(Collectors.toList())));
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(
            final MissingServletRequestParameterException ex,
            final HttpHeaders headers,
            final HttpStatus status,
            final WebRequest request) {
        return ResponseEntity
                .badRequest()
                .contentType(MediaType.APPLICATION_JSON)
                .body(new ErrorDTO(List.of(new FieldErrorDTO(ex.getParameterName(), "Parameter is missing"))));
    }

}
