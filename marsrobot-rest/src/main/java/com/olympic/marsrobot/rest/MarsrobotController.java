package com.olympic.marsrobot.rest;

import com.olympic.marsrobot.common.exception.NoBackOffStrategyFoundException;
import com.olympic.marsrobot.common.model.dto.ErrorDTO;
import com.olympic.marsrobot.common.model.dto.Input;
import com.olympic.marsrobot.common.model.dto.Output;
import com.olympic.marsrobot.common.service.MarsrobotService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Optional;

@RestController
public class MarsrobotController {

    private final MarsrobotService marsrobotService;

    public MarsrobotController(final MarsrobotService marsrobotService) {
        this.marsrobotService = marsrobotService;
    }

    @PostMapping(value = "/robot/run", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity runRobot(@Valid @RequestBody final Input input) {
        try {
            final Optional<Output> optionalOutput = marsrobotService.runRobot(input);
            return optionalOutput.isPresent()
                    ? ResponseEntity.ok(optionalOutput.get())
                    : ResponseEntity.badRequest().body(ErrorDTO.outOfTerrainErrorDTO(input.getInitialPosition().getLocation().getX(), input.getInitialPosition().getLocation().getY()));
        } catch (final NoBackOffStrategyFoundException e) {
            return ResponseEntity.ok(Output.fromRobot(e.getRobot()));
        }
    }

}
