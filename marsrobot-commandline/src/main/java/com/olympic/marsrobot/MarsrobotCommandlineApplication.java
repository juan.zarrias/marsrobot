package com.olympic.marsrobot;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.olympic.marsrobot.common.model.dto.ErrorDTO;
import com.olympic.marsrobot.common.model.dto.Input;
import com.olympic.marsrobot.common.model.dto.Output;
import com.olympic.marsrobot.common.service.MarsrobotService;
import com.olympic.marsrobot.common.service.MarsrobotServiceImpl;
import com.olympic.marsrobot.validation.CommandLineInputValidator;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Optional;

@SpringBootApplication
@Log4j2
@ComponentScan({"com.olympic.marsrobot.common.service"})
public class MarsrobotCommandlineApplication implements CommandLineRunner {

    @Value("${robot.fallback.strategy}")
    private String fallback;

    public static void main(String[] args) {
        final ConfigurableApplicationContext context = SpringApplication.run(MarsrobotCommandlineApplication.class, args);
        log.info("--> Shutting down robot...");
        context.close();
    }

    @Override
    public void run(final String... args) {
        if (args != null && args.length == 2) {
            try {
                final String request = Files.readString(Paths.get(args[0]));
                final Optional<ErrorDTO> optionalError = CommandLineInputValidator.validate(request);
                if (optionalError.isPresent()) {
                    log.error("--> Validation error found: [{}]", optionalError.get());
                } else {
                    final MarsrobotService marsrobotService = new MarsrobotServiceImpl(fallback);
                    final Input input = new ObjectMapper().readValue(request, Input.class);
                    final Optional<Output> optionalOutput = marsrobotService.runRobot(input);
                    if (optionalOutput.isPresent()) {
                        saveOutputToFile(optionalOutput.get(), args[1]);
                    } else {
                        final ErrorDTO errorDTO = ErrorDTO.outOfTerrainErrorDTO(input.getInitialPosition().getLocation().getX(), input.getInitialPosition().getLocation().getY());
                        log.error("--> Validation error found: [{}]", errorDTO);
                    }
                }
            } catch (final IOException e) {
                log.error("--> Error opening input file in given path. Does the file exist?");
            }
        } else {
            log.error("--> Program must be called with 2 parameters");
        }
    }

    private void saveOutputToFile(final Output output, final String destination) throws IOException {
        try {
            Files.writeString(Paths.get(destination), output.toString(), StandardOpenOption.CREATE_NEW);
        } catch (final IOException e) {
            log.error("--> Error writing destination file in given path. Is the path correct?");
        }
    }
}
