package com.olympic.marsrobot.validation;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.olympic.marsrobot.common.model.dto.ErrorDTO;
import com.olympic.marsrobot.common.model.dto.FieldErrorDTO;
import com.olympic.marsrobot.common.model.dto.Input;
import lombok.extern.log4j.Log4j2;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Log4j2
public class CommandLineInputValidator {

    public static Optional<ErrorDTO> validate(final String inputString) {
        try {
            final Input input = new ObjectMapper().readValue(inputString, Input.class);
            final Set<ConstraintViolation<Input>> constraintViolations = validateInput(input);
            if (constraintViolations.isEmpty()) {
                return Optional.empty();
            } else {
                return Optional.of(new ErrorDTO(
                        constraintViolations
                                .stream()
                                .map(c -> new FieldErrorDTO(c.getPropertyPath().toString(), c.getMessageTemplate()))
                                .collect(Collectors.toList())));
            }
        } catch (final Exception e) {
            log.error("--> Input Json doesn't have the correct structure");
            return Optional.of(new ErrorDTO(List.of(new FieldErrorDTO("input", "Input Json doesn't have the correct structure"))));
        }
    }

    private static Set<ConstraintViolation<Input>> validateInput(final Input input) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        return validator.validate(input);
    }
}
