FROM openjdk:11-jdk-slim

MAINTAINER jzarrias@gmail.com

COPY marsrobot-rest/target/marsrobot-rest-0.0.1-SNAPSHOT.jar app.jar

CMD ["java", "-jar", "/app.jar"]